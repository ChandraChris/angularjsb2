var app = angular.module('myApp', []);

app.controller('Siswa', function ($scope) {
    $scope.ordering = function (ordvar, by) {
        ordvar = !ordvar;
        $scope.ordstatus = ordvar;
        $scope.ord = by;
        return ordvar;
    }
    $scope.Siswa = [{
        name: "Joko",
        class: "Arsitek",
        score: 40
    }, {
        name: "Nami",
        class: "Pariwisata",
        score: 50
    }, {
        name: "Luffy",
        class: "Komedi",
        score: 90
    }, {
        name: "Zoro",
        class: "Samurai",
        score: 70
    }, {
        name: "Sanji",
        class: "Tata Boga",
        score: 90
    }, {
        name: "Chooper",
        class: "Kedokteran",
        score: 87
    }, {
        name: "Brook",
        class: "Music",
        score: 90
    }, {
        name: "Franky",
        class: "Mesin",
        score: 79
    }, {
        name: "Robin",
        class: "Tata Rias",
        score: 86
    }, {
        name: "Jinbei",
        class: "Perkapalan",
        score: 97
    }, {
        name: "Souma",
        class: "Perkapalan",
        score: 93
    }, {
        name: "Joichiro",
        class: "Perkapalan",
        score: 99
    }, ];
});
var searchField = $('#search');
var icon = $('#icon');
var thead = $('#thead');
searchField.mouseenter(function () {
    icon.attr('class', 'btn btn-success');
});
searchField.mouseleave(function () {
    icon.attr('class', 'input-group-text');
});
thead.mouseenter(function () {
    thead.attr('class', 'thead-light');
});
thead.mouseleave(function () {
    thead.attr('class', 'thead-dark');
});